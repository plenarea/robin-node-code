var server = require('../server.js');
var request = require('request');
var cfg = require('../server/config');

var magentoGatewayURL = cfg.hostname + ':' + cfg.port +  cfg.magentocreateorder;

describe("Submit a place order to Magento", function(){
    it("Submit a place order to Magento", function(done) {

        var payload =
            {
                "customer":
                    {
                        "firstname": "Swati",
                        "lastname": "Choudhari",
                        "email": "swatchoudhari@gmail.com",
                        "phone": "(512) 555-1111",
                        "shipzip": "90344",
                        "shipcity": "Alabama",
                        "shipstate": "AL",
                        "shipaddress1" : "123 Oak Ave",

                        "billzip": "90344",
                        "billcity": "Alabama",
                        "billstate": "AL",
                        "billaddress1" : "123 Oak Ave",

                        "shipping_method_code": "flatrate",
                        "shipping_carrier_code": "flatrate"
                    },
                "cart":
                    {
                        "shippingMethod": "FEDEX_2_DAY_EXPRESS",
                        "items": [
                            {
                                "sku":"24-MB01",
                                "qty":10,
                                "name":"Joust Duffle Bag",
                                "price":50.00,
                                "product_type":"simple",
                                "quote_id": ""
                            }
                        ]
                    }

            };



        request.post({
            headers: {
                'Authorization': cfg.endpoint.authtoken,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            url: magentoGatewayURL,
            body: payload,
            json: true
        }, function (error, response, body) {
            expect(response.statusCode).toBe(200);
            done();
        });
    });

    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

});