var request = require("request");
var server = require("../server.js");
var cfg = require('../server/config');

var base_url = cfg.hostname + ':' + cfg.port + cfg.endpoint.ping;

describe("PING our API ", function() {
    describe("GET and look for code 200", function() {
        it("returns status code 200", function(done) {
            request.get(base_url, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                done();
            });
        });

        it("GET and look for ok in the body", function(done) {
            request.get(base_url, function(error, response, body) {
                expect(body).toBe('ok\n');
                server.closeit();
                done();
            });
        });
    });
});