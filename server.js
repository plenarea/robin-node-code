var Hapi = require('hapi');
var hapiAuthJWT = require('hapi-auth-jwt2');
var inert = require('inert');
var vision = require('vision');
var cfg = require('./server/config');
var logger = require('./server/utility/logger');
var bifRoutes = require('./server/controllers/BifRoutes');
var ecnsRoutes = require('./server/controllers/EcnsRoutes');
var orderRoutes = require('./server/controllers/OrderRoutes');
var securepayRoutes = require('./server/controllers/SecurePayRoutes');
var magentoRoutes = require('./server/controllers/MagentoRoutes');


var validate = function (decoded, request, callback) {
    logger.debug("server long token", decoded);
    return callback(null, true);   //TODO: RSG 11/07/2017 change later
};


var server = new Hapi.Server();
server.connection({port:cfg.port, routes: {cors: true}});

// First make sure we have a loopback ping test ready
server.route({
    method: 'GET',
    path: '/ping',
    handler: function (request, reply) {
        logger.debug('ping received');
        reply('ok\n').code(200);
    }
});

server.register([
    {
        // register logging plugins
        register: require('good'),
        options: {
            reporters: {
                console: [
                    {
                        module: 'good-squeeze',
                        name: 'Squeeze',
                        args: [{response: '*', log: '*'}]
                    },
                    {
                        module: 'good-console'
                    },
                    'stdout'
                ]
            }
        }
    },
    {
        register: hapiAuthJWT
    },
    {
        register: inert
    },
    {
        register: vision
    },
    {
        register: require('hapi-swagger')
    },
    {
        register: require('hapijs-status-monitor')
    }

],function (err) {
    server.auth.strategy('jwt', 'jwt',
        { key: 'admin',  validateFunc: validate,
            verifyOptions: { algorithms: [ 'HS256' ] }
        });

    server.route(bifRoutes);
    server.route(ecnsRoutes);
    server.route(orderRoutes);
    server.route(securepayRoutes);
    server.route(magentoRoutes);
});

server.start(function(){
    server.log(['debug'], 'server running at:'+server.info.uri);
    logger.info('server running at: ' + server.info.uri);
});


module.exports = server;
module.exports.closeit = function () {
    server.stop();
};
