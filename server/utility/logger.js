'use strict';
var cfg = require('../config');
const fs = require('fs');
const logDir = cfg.log.folder;

if (!fs.existsSync(logDir)) { 
    fs.mkdirSync(logDir);
} 
  
const winston = require('winston');
var date = new Date();
const timeFormat = () => (new Date()).toLocaleTimeString();
const datetimeFormat = () => date.getFullYear() + '-' +  (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.toLocaleTimeString();
 
winston.emitErrs = true;

const logger = new winston.Logger({
    transports: [
        new (require('winston-daily-rotate-file'))({ 
            filename: logDir + '/' + cfg.log.filename,
            timestamp: timeFormat,
            datePattern: cfg.log.date_pattern,
            prepend: true,
            level: cfg.log.level,
            json: false
        }),
        new winston.transports.Console({
            timestamp: datetimeFormat,
            level: cfg.log.level,
            handleExceptions: true,
            json: false,
            colorize: true
        })
    ],
    exitOnError: false
});

module.exports = logger;

module.exports.stream = {
    write: function(message, encoding){
        logger.info(message);
    }
};
