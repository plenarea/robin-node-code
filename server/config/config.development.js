var config = require('./config.global');

config.env = 'development';
config.hostname = 'dev.peach';
config.port = 3000;

config.log.level = 'silly';

config.magento.host = 'http://ec2-13-126-101-114.ap-south-1.compute.amazonaws.com/magento2';
config.magento.auth_token = 'koglnh97fmt31o3n9ku8u5qei89nqsrr';
config.magento.programId = 'AmexConsumer';

config.magento.product.packageId = '3603';
config.magento.product.offerCode = 'AMGCB';

config.bif.host = 'http://sdbifapi280v:8080';
config.bif.shaKey = '';
config.bif.clientId = 'ecomm';

config.ecns.host = 'http://10.4.6.56:8080';
config.ecns.fromAddress = 'noreply@myvanilla.com';

config.securepay.host = 'https://uatsecurepay.incomm.com';
config.securepay.tx_config = 'eComm';
config.securepay.tx_payment_config = 'AmexSandbox2';
config.securepay.on = false;

config.database.host = '';
config.database.username = '';
config.database.password = '';
config.database.dbname = '';

module.exports = config;
