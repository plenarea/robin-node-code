var config = require('./config.global');

config.env = 'production';
config.hostname = 'amex.peach';
config.port = 3000;

config.log.level = 'error';

config.magento.host = '';
config.magento.auth_token = '';
config.magento.programId = '';

config.magento.product.packageId = '';
config.magento.product.offerCode = '';

config.bif.host = '';
config.bif.shaKey = '';
config.bif.clientId = '';

config.ecns.host = '';
config.ecns.fromAddress = 'NoReply@orders.amexgiftcard.com';

config.securepay.host = '';
config.securepay.tx_config = '';
config.securepay.tx_payment_config = '';
config.securepay.on = true;

config.database.host = '';
config.database.username = '';
config.database.password = '';
config.database.dbname = '';

module.exports = config;