var config = require('./config.global');

config.env = 'test';
config.hostname = 'http://localhost';
config.port = 3000;

config.log.level = 'info';

config.endpoint = {};
config.endpoint.authtoken = '123test';
config.endpoint.ping = '/ping';
config.endpoint.magentocreateorder = '/magento/checkout/cart';
config.endpoint.sendemail = '/ecns/sendEmail';
config.endpoint.submitbiforder = '/bif/order';
config.endpoint.submitbiforderId = '3';
config.endpoint.receivebifnotification = '/bif/order/notification';

config.magento.host = 'http://10.42.16.197';
config.magento.auth_token = '';
config.magento.programId = 'AmexConsumer';

config.magento.product.packageId = '3603';
config.magento.product.offerCode = 'AMGCB';

config.bif.host = 'http://sdbifapi280v:8080';
config.bif.shaKey = '';
config.bif.clientId = 'ecomm';

config.ecns.host = 'http://10.4.6.56:8080';
config.ecns.fromAddress = 'noreply@myvanilla.com';
config.ecns.toAddress = 'noreply@incomm.com';
config.ecns.referenceType = 'Test AmEx BOL Order';

config.securepay.host = 'https://uatsecurepay.incomm.com';
config.securepay.tx_config = 'eComm';
config.securepay.tx_payment_config = 'AmexSandbox2';
config.securepay.on = false;

config.database.host = '';
config.database.username = '';
config.database.password = '';
config.database.dbname = '';

module.exports = config;
