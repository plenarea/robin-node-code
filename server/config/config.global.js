var config = module.exports = {};

config.env = '';
config.hostname = '';
config.port = 3000;

config.log = {};
config.log.level = 'debug'; // npm logging levels { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }
config.log.folder = '/tmp/logs';
config.log.filename = '-ecomm-gateway.log';
config.log.date_pattern = 'yyyy-MM-dd';

config.magento = {};
config.magento.host = '';
config.magento.base_uri = '/rest/V1'; 
config.magento.card_uri = '/cards';
config.magento.auth_token = '';
config.magento.programId = '';
config.magento.orderPrefix = 'M';
config.magento.orderMissingMessage = 'Order not found in Magento';

config.magento.product = {};
config.magento.product.packageId = '';
config.magento.product.offerCode = '';

config.bif = {};
config.bif.host = '';
config.bif.base_uri = '/services/order/api/createOrder';
config.bif.shaKey = '';
config.bif.clientId = '';
config.bif.clientOrderBatchPrefix = '7';
config.bif.orderStatus = {PENDING: 'PENDING', RECEIVED: 'RECEIVED', REJECTED: 'REJECTED', PROCESSED: 'PROCESSED', SHIPPED: 'SHIPPED', COMPLETED: 'COMPLETED'};

config.ecns = {};
config.ecns.host = '';
config.ecns.base_uri = '/ecns-rs/rs/ecns/notificationSend';
config.ecns.referenceType = 'AmEx BOL Order';

config.securepay = {};
config.securepay.host = '';
config.securepay.base_uri = '/api/transaction';
config.securepay.tx_config = '';
config.securepay.tx_payment_config = '';
config.securepay.on = false;
config.securepay.tx_iv_length = 8;
config.securepay.tx_storepaymentprofile = 'N';
config.securepay.tx_type_authonly = 'AUTHONLY';
config.securepay.tx_type_refund = 'REFUND';
config.securepay.tx_type_void = 'VOID';
config.securepay.tx_type_priorauth = 'CAPTUREPRIORAUTH';
config.securepay.tx_currency_code = 'USD';

config.database = {};
config.database.dialect = 'mysql';
