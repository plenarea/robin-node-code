var magentoResource = require('../brokers/MagentoResource');
var logger = require('../utility/logger');

module.exports={

    /*
    * Get a order using Magento REST API
    */
    getMagentoOrderById: function(request, response) {
        var orderId = request.params.orderid;
        magentoResource.getMagentoOrderById(orderId, function(data) {
            response(data).code(200);
        });
    },

    /*
    * Get all Order comments using Magento REST API
    */
    getMagentoOrderCommentsByOrderId: function(request, response) {
        var orderId = request.params.orderid;
        magentoResource.getMagentoOrderCommentsByOrderId(orderId, function(data) {
            response(data).code(200);
        });
    },

    /*
    * List all the products in Magento
    */
    findAllMagentoProducts: function(request,response) {
        var data='test';
        magentoResource.findAllCatalogItems(data, function (cdata) {
            response(cdata).code(200);
        });
    },

    /*
    * List categorywise products in Magento
    */
    findAllMagentoCategorywiseProducts: function(request,response) {
        var categoryId = request.params.categoryid;
        magentoResource.findAllCategorywiseProducts(categoryId, function (cdata) {
            response(cdata).code(200);
        });
    },


    /*
    * Submit a cart to Magento at check out. This will turn into a Magento order. Need items
    */
    submitCart: function(request,response) {
        var access_token = request.headers.authorization;
        var payload = request.payload;

        magentoResource.getCustomerToken(function(tokendata) {
                logger.info('Customer token=' + tokendata);
                magentoResource.createCart(tokendata, function(data) {
                    var cart_id = data;
                    logger.info('New cart_id=' + cart_id);
                    payload.cart.items.forEach(function(item) {
                        magentoResource.addItem2Cart(tokendata,cart_id, item, function(adata) {
                            logger.info('Adding item to the cart..' + adata);
                        });
                    });

                    magentoResource.estimateShippingDetailsofCart(tokendata, cart_id, payload.customer, function(bdata) {
                        logger.info(bdata);
                        magentoResource.addShippingAddress2Cart(tokendata, cart_id, payload.customer, function(cdata) {
                                logger.info(cdata);
                                magentoResource.createOrder(tokendata, cart_id,payload.customer, function(ddata) {
                                    logger.info(ddata);
                                    response(ddata).code(200);
                                });
                            });
                        });

                });
        });

    },

};
