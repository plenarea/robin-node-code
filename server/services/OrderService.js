var orderResource = require('../brokers/OrderResource');

module.exports={

    /*
    * Process BIF incoming notifications
    */
    processNotification: function(request, response) {

        var headers = request.headers;
        var jsonOrder = request.payload;

        orderResource.processNotification(jsonOrder, function(data) {
            response(data).code(200);
        });
    }

};
