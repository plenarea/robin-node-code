var securePayResource = require('../brokers/SecurePayResource');

module.exports={

    /*
     * Return the securepay iframe link src attribute
     */
    getSecurePayURL: function (request, response) {

        var headers = request.headers;
        var payload = request.payload;
        securePayResource.getSecurePayURL(payload, function (data) {
            response(data).code(200);
        });
    },

    /*
    * Process saving the auth details from SecurePay
    */
    processSaveAuth: function (request, response) {

        var access_token = '';
        var headers = request.headers;
        var payload = request.payload;

        var authtoken = payload.authtoken;
        var transactionnum = payload.transactionnum;
        var amount = payload.amount;
        var order_id = payload.order_id;
        var timestamp = payload.timestamp;
        securePayResource.processSaveAuth(request.payload, function (data) {
            response(data).code(200);
        });
    },

    /*
    * Post Partial Payment.
    */
    postPartialPayment: function (request, response) {
        securePayResource.postPartialPayment(request.payload, function (data) {
            response(data).code(200);
        });
    },

    /*
     * Post a refund.
     */
    postRefund: function (request, response) {
        securePayResource.postRefund(request.payload, function (data) {
            response(data).code(200);
        });
    },

    /*
     * Post a void.
     */
    postVoid: function (request, response) {
        securePayResource.postVoid(request.payload, function (data) {
            response(data).code(200);
        });
    },

    /*
     * Process credit card auth. This is a mock
     */
    processCreditCardAuth: function (request, response) {

        var access_token = '';
        var headers = request.headers;
        var payload = request.payload;
        var creditcard = payload.creditcard;
        var expiration = payload.expiration;
        var securitycode = payload.securitycode;
        var nameoncard = payload.nameoncard;
        var zip = payload.zip;

        var data = { 'authtoken': '1213', 'transactionnum': '14567' };
        response(data).code(200);
    },

    /*
     * Process credit card transact. This is a mock
     */
    processCreditCardPay: function (request, response) {

        var access_token = '';
        var headers = request.headers;
        var payload = request.payload;
        var authtoken = payload.authtoken;
        var transactionnum = payload.transactionnum;
        var amount = payload.amount;
        var data = { 'message': 'OK' };
        response(data).code(200);
    }

};