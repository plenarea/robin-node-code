var cfg = require('../config/index');
var magentoResource = require('../brokers/MagentoResource');
var bifResource = require('../brokers/BifResource');

module.exports={

    /*
     * Sending a Magento Order to BIF for processing by downstream systems 
     */
    sendMagentoOrder: function(request,response)  {

        var access_token = request.headers.authorization;
        var payload = request.payload;
        var jsonOrder = payload.order;

        magentoResource.getMagentoOrderById(jsonOrder, function (data) {
            var message = JSON.stringify(data);
            if(message.match('Requested entity')) {
                response({'responseCode': '01', 'responseText': cfg.magento.orderMissingMessage, 'srcTXNID': 'Future use'}).code(200);
            } else {
                bifResource.sendMagentoOrder(access_token, data, function (cdata) {
                    response(cdata).code(200);
                });
            }
        });
    }

};

