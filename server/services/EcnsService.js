
var ecnsResource = require('../brokers/EcnsResource');

module.exports={

    sendEmail: function(request,response)  {

        var access_token = '';
        var headers = request.headers;
        
	    access_token = headers.authorization;
        
        var payload = request.payload;
        var jsonOrder = payload;

        ecnsResource.sendEmail(access_token,jsonOrder,function (data) {
            response(data).code(200);
        });
    }

};
