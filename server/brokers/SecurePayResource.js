/**
 * Created by rsg on 11/20/2017.
 */
var cfg = require('../config/index');
var logger = require('../utility/logger');
var myip = require('../utility/ip');
var request = require('request');
var crypto = require('crypto');
var CryptoJS = require('crypto-js');

module.exports = {

    /*
    *  Post partial payment (charge the customer) via SecurePay.
    *
    *  CALLER: Order flow.
    *  Call should originate from order flow, when BIF notifies us that a given order/line item is 'SHIPPED' //TODO: COMPLETED ??
    *  We need to go the Magento via REST API and get the auth token/ line item x's amount. (Later find out where is this token from Chris)
    *  Then call this service { order_id : xxx, authtoken : yyyy , amount : zzzz }
    *
    * INPUTS:  json payload object with order_id, authtoken, amount
    * OUTPUTS: Returns json object with securepay iframe link src attribute.
    *
    */
    postPartialPayment: function (payload, callback) {


        var tx_sequence_id = payload.order_id;  // Magento order reference #
        var from = tx_sequence_id + '@' + cfg.securepay.tx_config + '.' + cfg.securepay.tx_payment_config;

        var tx_fingerprint = CryptoJS.SHA256([cfg.securepay.tx_config, cfg.securepay.tx_payment_config, tx_sequence_id, this.getTxTransdataIV(cfg.securepay.tx_iv_length)].join(","));

        var ip = null;
        if (myip.IP != null) {
            ip = myip.IP[0];
        }

        request.put({
            url: cfg.securepay.host + cfg.securepay.base_uri + '/' + payload.authtoken,
            headers: {
                'Authorization': tx_fingerprint.toString(),
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'From': from
            },
            body: {
                'Amount': payload.amount, 
                'CurrencyCode': cfg.securepay.tx_currency_code, 
                'TransType': cfg.securepay.tx_type_priorauth,
                'IPAddress': ip
            },
            json:true
        }, function(error, response, body){
            logger.debug('Response from SecurePay -- ' + JSON.stringify(response));
            if (error) {
                logger.error('PostPartiaPayment error: ' + JSON.stringify(error));
                var errResponse = {'responseCode' : response.statusCode, 'error': error, 'response': response };
                callback(errResponse);
            }
            else {
                logger.debug('POST to SecurePay for partial payment is successful!!');
                callback(JSON.stringify(response));
            }
        });
    },


    /*
     *  Void a payment (void the charge) via SecurePay.
     *
     *  CALLER: Order flow.
     *  Call should originate from order flow.
     *
     * INPUTS:  json payload object with order_id, authtoken, amount
     * OUTPUTS: Returns json object from securepay.
     *
     */
    postVoid: function (payload, callback) {

        var response = [];
        var tx_sequence_id = payload.order_id;  // Magento order reference #
        var from = tx_sequence_id + '@' + cfg.securepay.tx_config + '.' + cfg.securepay.tx_payment_config;

        var tx_fingerprint = CryptoJS.SHA256([cfg.securepay.tx_config, cfg.securepay.tx_payment_config, tx_sequence_id, this.getTxTransdataIV(cfg.securepay.tx_iv_length)].join(","));

        var ip = null;
        if (myip.IP != null) {
            ip = myip.IP[0];
        }

        request.put({
            url: cfg.securepay.host + cfg.securepay.base_uri + '/' + payload.authtoken,
            headers: {
                'Authorization': tx_fingerprint.toString(),
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'From': from
            },
            body: {
                'Amount': payload.amount,
                'CurrencyCode': cfg.securepay.tx_currency_code,
                'TransType': cfg.securepay.tx_type_void,
                'IPAddress': ip
            },
            json:true
        }, function(error, response, body){
            logger.debug('Response from SecurePay -- ' + JSON.stringify(response));
            if (error) {
                logger.error('PostVoid error: ' + JSON.stringify(error));
                var errResponse = {'responseCode' : response.statusCode, 'error': error, 'response': response };
                callback(errResponse);
            }
            else {
                logger.debug('POST to SecurePay for void is successful!!');
                callback(JSON.stringify(response));
            }
        });
    },


    /*
     *  Refund a payment via SecurePay.
     *
     *  CALLER: Order flow.
     *  Call should originate from order flow.
     *
     * INPUTS:  json payload object with order_id, authtoken, amount
     * OUTPUTS: Returns json object from securepay.
     *
     */
    postRefund: function (payload, callback) {

        var response = [];
        var tx_sequence_id = payload.order_id;  // Magento order reference #
        var from = tx_sequence_id + '@' + cfg.securepay.tx_config + '.' + cfg.securepay.tx_payment_config;

        var tx_fingerprint = CryptoJS.SHA256([cfg.securepay.tx_config, cfg.securepay.tx_payment_config, tx_sequence_id, this.getTxTransdataIV(cfg.securepay.tx_iv_length)].join(","));

        var ip = null;
        if (myip.IP != null) {
            ip = myip.IP[0];
        }

        request.put({
            url: cfg.securepay.host + cfg.securepay.base_uri + '/' + payload.authtoken,
            headers: {
                'Authorization': tx_fingerprint.toString(),
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'From': from
            },
            body: {
                'Amount': payload.amount,
                'CurrencyCode': cfg.securepay.tx_currency_code,
                'TransType': cfg.securepay.tx_type_refund,
                'IPAddress': ip
            },
            json:true
        }, function(error, response, body){
            logger.debug('Response from SecurePay -- ' + JSON.stringify(response));
            if (error) {
                logger.error('PostRefund error: ' + JSON.stringify(error));
                var errResponse = {'responseCode' : response.statusCode, 'error': error, 'response': response };
                callback(errResponse);
            }
            else {
                logger.debug('POST to SecurePay for a refund is successful!!');
                callback(JSON.stringify(response));
            }
        });
    },


    /*
     *  Get the SecurePay Url link to be put dynamically on the IFRAME when user moves from Step 1 to Step 2 in the checkout
     *
     * INPUTS:  json payload object with order_id, amount
     * OUTPUTS: Returns json object with securepay iframe link src attribute.
     */
    getSecurePayURL: function (payload, callback) {

        var tx_sequence_id = payload.order_id;  // Magento order reference #

        var tx_fingerprint = CryptoJS.SHA1([cfg.securepay.tx_config, cfg.securepay.tx_payment_config, tx_sequence_id, this.getTxTransdataIV(cfg.securepay.tx_iv_length)].join(","));

        var tx_amount = payload.amount;         // Amount that we will be using on the iFrame

        var message = "tx_payment_config=" + cfg.securepay.tx_payment_config +
                        "&tx_sequence_id=" + tx_sequence_id +
                        "&tx_fingerprint=" + tx_fingerprint +
                        "&tx_storepaymentprofile=" +  cfg.securepay.tx_storepaymentprofile +
                        "&tx_type=" + cfg.securepay.tx_type_authonly +
                        "&tx_amount=" + tx_amount +
                        "&tx_currency_code=" + cfg.securepay.tx_currency_code;

        var tx_transdata = CryptoJS.TripleDES.encrypt(message, cfg.securepay.tx_transdataiv);

        var json = { 'securepay_iframe_url':  cfg.securepay.host + '/payment.aspx?tx_config=' + cfg.securepay.tx_config + 
                    '&tx_transdataiv=' + cfg.securepay.tx_transdataiv + '&tx_transdata=' + tx_transdata };

        logger.debug('JSON response -- ' + json);
        callback(json);
    },

    /*
     * Save Auth token as comment for the order in Magento
     */
    processSaveAuth: function (jsonOrder, callback) {

        var response = [];
        var id = jsonOrder.order_id;
        logger.debug('Saving Auth Token for OrderId: ' + id);

        request.post({
            url: cfg.magento.host + cfg.magento.base_uri + '/orders/' + id + '/comments',
            headers: {
                'Authorization': 'Bearer ' + cfg.magento.auth_token,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: {
                "statusHistory": {
                    "comment": 'AUTHTOKEN:' + jsonOrder.authtoken + ', TRANSACTIONNUM:' + jsonOrder.transactionnum + ', AMOUNT:' + jsonOrder.amount,
                    "is_visible_on_front": 1
                }
            },
            json:true
        }, function(error, response, body){
            logger.debug('Response from Magento -- ' + JSON.stringify(response));
            if (error) {
                logger.error('Magento SaveAuth error: ' + JSON.stringify(error));
                callback(response);
            }
            else {
                logger.debug('Saving of Auth Response into Magento successful..');
                callback(body);
            }
        });
    },

    /*
     * Get Base64 string for random 'n' bytes
     */
    getTxTransdataIV: function (numberOfBytes) {
        return crypto.randomBytes(numberOfBytes).toString('base64');
    }

};