var cfg = require('../config/index');
var logger = require('../utility/logger');
var request = require('request');


module.exports = {


    /*
     *  Send notification to ECNS system
     */
    sendEmail: function (auth_token, json, callback) {

        // Check for attachment binary content if attachment name is provided. These attributes are for future use.
        if(json.attachmentName != null) {
            if(typeof json.attachmentBinary == 'undefined') {
                return callback({'responseCode': '1', 'responseText': 'Attachment content missing!', 'srcTXNID': 'Future use'});
            }
        }

        var ecnsMessage = this.createECNSMessage(json);
        logger.debug('ecnsMessage=' + ecnsMessage);
        var ecns_uri = cfg.ecns.host + cfg.ecns.base_uri;
        request(ecns_uri,
            {
                method: 'POST',
                headers: {'Content-type': 'application/xml'},
                body: ecnsMessage
            },
            function (err, res, body) {
                if (err) {
                    logger.error('ECNS send email error -- ' + JSON.stringify(err));
                    callback({'responseCode': '1', 'responseText': res, 'srcTXNID': 'Future use'});
                } else {
                    logger.debug(body);
                    logger.info('ECNS send email successful!!');
                    var match = body.match(/<NotifRespMessage>([^<]*)<\/NotifRespMessage>/);
                    var respMessage = match[1];
                    match= body.match(/<CnsNotifRespRefNum>([^<]*)<\/CnsNotifRespRefNum>/);
                    var respRefId = match[1];
                    match= body.match(/<NotifRespCode>([^<]*)<\/NotifRespCode>/);
                    var respCode = match[1];

                    var message = {'responseCode': '' + respCode, 'responseText': respMessage, 'srcTXNID': '' + respRefId};
                    callback(message);
                }
            });
    },


    /*
     *  Create a ECNS final message to the caller
     */
    createECNSMessage: function (json) {

        var message = '<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><NotificationSendRequest xmlns=\"http://ecns.incomm.com/xmlmessage/xsd\">' +
            '<NotifRequestor><AppId>ddsemail</AppId><AppHostname>atldevtpscsc01</AppHostname></NotifRequestor>' +
            '<NotifRequestHeader><AppNotifType>' + json.reference_type + '</AppNotifType><AppNotifReqRefNum>' + json.reference_reqnum + 
             '</AppNotifReqRefNum></NotifRequestHeader><NotifRecipientList><NotifRecipient>' +
             '<NotifRecipientValue>' + json.to + '</NotifRecipientValue></NotifRecipient></NotifRecipientList><NotifMessage>' +
             '<NotifMessageType>email</NotifMessageType>' +
             '<NotifMessageContent>' + json.message +'</NotifMessageContent></NotifMessage>' + 
             '<NotifExtensionList><NotifExtension><name>from</name><value>' + json.from + '</value>' +
             '</NotifExtension><NotifExtension><name>subject</name><value>' + json.subject +'</value></NotifExtension></NotifExtensionList>' +
             '<NotifAttachementList><NotifAttachment><name>' + json.attachmentName + '</name><value>' + json.attachmentBinary + '</value></NotifAttachment></NotifAttachementList>' +
            '</NotificationSendRequest>';

        return message;
    }

};
