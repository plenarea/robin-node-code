/**
 * Created by rsg on 9/20/2017.
 */
var cfg = require('../config/index');
var logger = require('../utility/logger');
var request = require('request');
var securePayResource = require('./SecurePayResource');
var magentoResource = require('./MagentoResource');

module.exports = {

    /*
     *  Process notifications from BIF. Sends response from here
     */
    processNotification: function (jsonOrder, callback) {

        var orders = jsonOrder.orders;
        logger.debug('OrderResource: processNotification() with order -- ' + JSON.stringify(orders));
        var response = {'responseCode': '0', 'responseText': 'Success', 'srcTXNID': 'Future use'};
        var errorResponseCode = '1';
        var errorSrcTxnId = 'Update notification for order failed!';
        var orderId = null;

        orders.forEach(function (order) {
            orderId = order.orderId.substring(cfg.magento.orderPrefix.length);
        });

        magentoResource.getMagentoOrderById(orderId, function (data) {
            module.exports.processNotificationDetails(data, jsonOrder, function (data) {
                if (data === '{ok}' || JSON.stringify(data) === 'true') {
                    callback(response);
                }
                else {
                    var errMessage = JSON.stringify(data).replace(/\\/g, '');
                    errMessage = errMessage.replace(/\"/g, '');
                    var errResponse = {
                        'responseCode': errorResponseCode,
                        'responseText': errMessage,
                        'srcTXNID': errorSrcTxnId
                    };
                    callback(errResponse);
                }
            });
        });
    },


    /*
     * Process the order-suborder details
     */
    processNotificationDetails: function (magOrder, jsonOrder, callback) {

        var orders = jsonOrder.orders;
        var id = null;
        var lineId = null;
        var shippingStatus = null;

        orders.forEach(function (order) {
            id = order.orderId.substring(cfg.magento.orderPrefix.length);
            shippingStatus = order.shippingStatus;
            logger.debug('OrderId: ' + id + ', shippingStatus: ' + shippingStatus);

            // Check for subOrders before forEach
            if (order.subOrders != null) {
                order.subOrders.forEach(function (lineitem) {
                    lineId = lineitem.subOrderId;
                    var lineShippingStatus = lineitem.shippingStatus;
                    logger.debug('SubOrderId: ' + lineId + ', shippingStatus: ' + lineShippingStatus);
                });
            }
        });


        // As discussed with BIF team, updating card details for Processing, Shipping, Completed with case-insensitive check
        if ((shippingStatus.toUpperCase() === cfg.bif.orderStatus.RECEIVED) || (shippingStatus.toUpperCase() === cfg.bif.orderStatus.REJECTED)) {
            logger.info('Order with ' + shippingStatus + ' status handled successfully!!');
            return callback('{ok}');
        }
        if ((shippingStatus.toUpperCase() === cfg.bif.orderStatus.PROCESSED) || (shippingStatus.toUpperCase() === cfg.bif.orderStatus.SHIPPED)
            || (shippingStatus.toUpperCase() === cfg.bif.orderStatus.COMPLETED)) {
            logger.debug('Order is in ' + shippingStatus + ' status, updating cards if available...');
            module.exports.updateOrderWithCardDetails(jsonOrder, function (data) {
                logger.info('Order with ' + shippingStatus + ' status handled successfully!!' + data);
                callback(data);
            });
        }
        if (shippingStatus.toUpperCase() === cfg.bif.orderStatus.COMPLETED) {
            module.exports.updateOrderMainStatus(id, lineId, function (data) {
                logger.debug('Data in OrderResource - from updateOrderMainStatus -- ' + JSON.stringify(data));
            });
        }

    },


    /*
     * Update the status of the order at the Order level in Magento
     */
    updateOrderMainStatus: function (id, lineId, callback) {

        request.post({
            url: cfg.magento.host + cfg.magento.base_uri + '/orders',
            headers: {
                'Authorization': 'Bearer ' + cfg.magento.auth_token,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: {
                'entity': {
                    'entity_id': id,
                    'status': 'complete'
                }
            },
            json: true
        }, function (error, response, body) {
            logger.debug('Response from Magento -- ' + JSON.stringify(response));
            if (error) {
                logger.error('Error -- ' + JSON.stringify(error));
                callback(response);
            }
            else {
                logger.debug('Current status: ' + body.status);
                logger.debug('Number of items: ' + body.items.length);
                var items = body.items;
                var amount = 0.0;
                var is = '';
                for (var i = 0; i < items.length; i++) {
                    is = '' + (i + 1);
                    var obj = items[i];
                    logger.debug('Item #: ' + (i + 1));
                    logger.debug('Row_total: ' + obj.row_total);
                    logger.debug('Updated At: ' + obj.updated_at);
                    amount = obj.row_total;
                    logger.debug('Amount: ' + amount);
                    if (lineId === is) {
                        logger.debug('Matched amount: ' + amount);
                        break;
                    }
                }
                var authtoken = '';
                var comments = body.status_histories;

                for (var i = 0; i < comments.length; i++) {
                    var obj = comments[i];
                    var comment = obj.comment;
                    logger.debug('Current comment: ' + comment);
                    if (comment.indexOf('AUTHTOKEN') === -1) {
                        i++;
                    }
                    else {
                        logger.debug('Matched AUTHTOKEN in the comment: ' + comment);
                        var tokens = comment.split(',');
                        var authtoken = tokens[0];
                        var index = authtoken.indexOf(':') + 1;
                        authtoken = authtoken.substring(index);

                        var transactionnum = tokens[1];
                        index = transactionnum.indexOf(':') + 1;
                        transactionnum = transactionnum.substring(index);

                        var amount = tokens[2];
                        index = amount.indexOf(':') + 1;
                        amount = amount.substring(index);

                        logger.debug('authtoken: ' + authtoken + ',transactionnum=' + ',amount=' + amount);
                        logger.debug('POST to Magento update order main status=complete successful..');
                        logger.info('Submit payment to SecurePay for authtoken: ' + authtoken + ', lineId: '
                            + lineId + ', amount: ' + amount);
                        break;
                    }
                }


                if (cfg.securepay.on) {
                    if (authtoken !== null && authtoken !== '') {
                        module.exports.postPartialPayment(id, lineId, authtoken, amount, function (data) {
                            logger.debug('Payment data is' + data);
                            callback(data);
                        });
                    }
                }
            }
        });
    },


    postPartialPayment: function (id, lineId, authtoken, amount, callback) {
        logger.debug('OrderResource: postPartialPayment() for OrderId: ' + id + ', with lineId: ' + lineId);
        var json = {'authtoken': authtoken, 'amount': amount, 'order_id': id};
        securePayResource.postPartialPayment(json, function (data) {
            callback(data);
        });
    },


    /*
     * Update card details in Magento based on the status of the order
     */
    updateOrderWithCardDetails: function (jsonOrder, callback) {

        var orders = jsonOrder.orders;
        var cards = [];
        var id;
        var lineId;
        var shipperName;            // stores shippingMethod from root
        var trackingNumber = '';    // from cards
        var shippingTimeStamp = ''; // from cards

        orders.forEach(function (order) {
            id = order.orderId.substring(cfg.magento.orderPrefix.length);
            shipperName = order.shippingMethod;
            shipperName = shipperName.substring(0, shipperName.indexOf('_'));
            logger.debug('OrderId: ' + id);

            // Check for subOrders before forEach
            if (order.subOrders !== null) {
                order.subOrders.forEach(function (lineitem) {
                    lineId = lineitem.subOrderId;
                    logger.debug('Processing line item # ' + lineId);

                    // Check for cards before forEach
                    if (lineitem.cards !== null) {
                        lineitem.cards.forEach(function (card) {
                            // Save card details only when serial number is present
                            if (card.serialNumber !== null) {
                                var serialNumber = card.serialNumber;
                                var proxyNumber = card.proxyNumber;
                                var pin = card.pin;
                                var shippingStatus = card.shippingStatus;
                                var encryptedString = '';

                                if (card.encryptedString !== null) {
                                    encryptedString = card.encryptedString;
                                }
                                if (card.trackingNumber !== null) {
                                    trackingNumber = card.trackingNumber;
                                }
                                if (card.shippingTimeStamp !== null) {
                                    shippingTimeStamp = card.shippingTimeStamp;
                                }

                                cards.push({
                                    order_id: id,
                                    lineitem_id: lineId,
                                    serial_number: serialNumber,
                                    status: shippingStatus,
                                    proxy_number: proxyNumber,
                                    pin: pin,
                                    encrypted_string: encryptedString,
                                    tracking_number: trackingNumber,
                                    shipper_name: shipperName,
                                    shipping_timestamp: shippingTimeStamp
                                });

                                logger.debug('Card -- OrderId: ' + id + ', LineId: ' + lineId + ', Status: ' + shippingStatus
                                    + ', Shipper: ' + shipperName + ', Tracking: ' + trackingNumber + ', ShippingTime: ' + shippingTimeStamp);
                            }
                            else {
                                logger.info('Card details does not include serial number.');
                            }

                        });
                        // Send the list of cards to Magento via custom API
                        module.exports.saveCards(cards, function (data) {
                            var message = JSON.stringify(data);
                            logger.debug('Card response save status from Magento: ' + message);
                        });
                    }
                });
            }
        });
        callback('{ok}');
    },

    /*
    * Send card details to another external API hosted in Magento.
    */
    saveCards: function (cards, callback) {

        logger.debug('Attempting to communicate with Magento Incomm Custom Rest API with cards ' + JSON.stringify(cards));
        request({
            headers: {
                'Authorization': 'bearer ' + cfg.magento.auth_token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            uri: cfg.magento.host + cfg.magento.card_uri,
            method: 'POST',
            body:  cards,
            json: true
        }, function (err, res, body) {
            if (err) {
                callback(response);
                logger.error('Submit cards to Magento failed! ' + JSON.stringify(err));
            }
            else {
                logger.info('Submit cards to Magento successful!!');
                logger.debug('Response -- ' + JSON.stringify(res));
                callback(body);
            }
        });
    }
};