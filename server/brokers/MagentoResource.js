/**
 * Created by rsg on 9/20/2017.
 */
var cfg = require('../config/index');
var logger = require('../utility/logger');
var request = require('request');


module.exports = {


    /*
     * Get Magento order by orderId
     */
    getMagentoOrderById: function(orderId, callback) {

        request(cfg.magento.host + cfg.magento.base_uri + '/orders/' + orderId,
            {
                json: true,
                method: 'GET',
                headers: {'Authorization': 'bearer ' + cfg.magento.auth_token, 'Content-type': 'application/json'}
            },
            function(err, res, body) {
                if (err) {
                    logger.error('Order API - Cannot communicate to MAGENTO');
                    callback(res);
                }
                if(body.customer_email) {
                    logger.debug('Order API - Order found -- Email: ' + body.customer_email
                        + ', Total: ' + body.total_due + ', Qty: ' + body.total_qty_ordered);
                } else {
                    logger.debug('Order API - Order not found -- Message: ' + body.message);
                }
                callback(body);
            });
    },


    /*
     * Get comments for Magento order by orderId
     */
    getMagentoOrderCommentsByOrderId: function(orderId, callback) {

        request(cfg.magento.host + cfg.magento.base_uri + '/orders/' + orderId + '/comments',
            {
                json: true,
                method: 'GET',
                headers: {'Authorization': 'bearer ' + cfg.magento.auth_token, 'Content-type': 'application/json'}
            },
            function(err, res, body) {
                if (err) {
                    logger.error('Order comments API - Cannot communicate to MAGENTO');
                    callback(res);
                }
                logger.debug('Order comments API - Number of comments -- ' + body.total_count);
                callback(body);
            });
    },


    /*
    *  Get all products from Magento Server via REST API. Sends response from here
    */
    findAllProducts: function (category_id,reply) {
        logger.info('Find products with category id: ' + id);
        var response = {'message': 'findAllProducts..', 'error': 0};

        module.exports.findAllCatalogItems(category_id, function (data) {
            reply(data).code(200);
        });
    },


    findAllCatalogItems: function (searchString, callback) {

        var productList = [];
        var productstatus = 1; //to get active product
        request(cfg.magento.host + cfg.magento.base_uri  + '/products?searchCriteria[page_size]=30&searchCriteria[filterGroups][0][filters][0][field]=status& searchCriteria[filterGroups][0][filters][0][value]='+productstatus+'&searchCriteria[filterGroups][0][filters][0][conditionType]=eq',
            {
                json: true, method: 'GET',
                headers: {'Authorization': 'bearer ' + cfg.magento.auth_token, 'Content-type': 'application/json'}
            },
            function (err, res, body) {
                if (err) {
                    logger.error('Product API - Cannot communicate to MAGENTO');
                    return;
                }
                var items = body.items;
                var json=JSON.stringify(body.items);

                if (body.items[0] == null) {logger.info('Empty'); return; }     // Check for empty links
                var counter=1;
                for (var item in items) {
                    var obj = items[item];
                    var Product = {};
                    Product['id'] = obj.id;
                    Product['epid'] = obj.id;
                    Product['name'] = obj.name;
                    Product['retail_price'] = obj.price;
                    Product['sale_price'] = obj.price;
                    Product['skuid'] = obj.sku;
                    Product['active'] = obj.status;
                    Product['availability'] = '';
                    Product['description'] = '';
                    Product['created_at'] = obj.created_at;
                    //RSG added
                    Product['promotion'] = 1;
                    Product['size'] = 'Standard';
                    Product['configuration'] = '16 GB';
                    Product['color'] = '';
                    Product['picture'] = '';
                    Product['active'] = 1;
                    Product['contract'] = '24 months';
                    Product['store'] = 'T-Mobile';

                    // get the attributes
                    var attributes = obj.custom_attributes;
                    for (var it in attributes) {
                        var ob = attributes[it];

                        /* This code changes based on the version of Magento */
                        if (ob.atrribute_code && ob.atrribute_code.indexOf('thumbnail') > 0)       //TODO: RSG do not know how to get the path to the media folder
                            Product[ob.attribute_code] =cfg.magento.host  + '/pub/media/catalog/product/cache/image/265x265/beff4985b56e3afdbeabfc89641a4582/' + ob.value;

                        if (ob.attribute_code.indexOf('image') > 0)       //TODO: RSG do not know how to get the path to the media folder
                            Product[ob.attribute_code] =cfg.magento.host  + '/pub/media/catalog/product/cache/image/265x265/beff4985b56e3afdbeabfc89641a4582/' + ob.value;
                    }
                    productList.push(Product);
                    counter++;
                }
                callback(productList);
            });
    },

    /*
    * Find categorywise products
    */
    findAllCategorywiseProducts: function (categoryId, callback) {

        var productList = [];

        var productstatus = 1; //to get active product
        request(cfg.magento.host + cfg.magento.base_uri  + '/products?searchCriteria[page_size]=30&searchCriteria[filterGroups][0][filters][0][field]=category_id& searchCriteria[filterGroups][0][filters][0][value]='+categoryId+'&searchCriteria[filterGroups][0][filters][0][conditionType]=eq&searchCriteria[filterGroups][0][filters][1][field]=status& searchCriteria[filterGroups][0][filters][1][value]='+productstatus+'&searchCriteria[filterGroups][0][filters][1][conditionType]=eq',
            {
                json: true, method: 'GET',
                headers: {'Authorization': 'bearer ' + cfg.magento.auth_token, 'Content-type': 'application/json'}
            },
            function (err, res, body) {
                if (err) {
                    logger.error('Product API - Cannot communicate to MAGENTO');
                    return;
                }
                var items = body.items;
                var json=JSON.stringify(body.items);

                if (body.items[0] == null) {logger.info('Empty'); return; }     // Check for empty links
                var counter=1;
                for (var item in items) {
                    var obj = items[item];
                    var Product = {};
                    Product['id'] = obj.id;
                    Product['epid'] = obj.id;
                    Product['name'] = obj.name;
                    Product['retail_price'] = obj.price;
                    Product['sale_price'] = obj.price;
                    Product['skuid'] = obj.sku;
                    Product['active'] = obj.status;
                    Product['availability'] = '';
                    Product['description'] = '';
                    Product['created_at'] = obj.created_at;
                    //RSG added
                    Product['promotion'] = 1;
                    Product['size'] = 'Standard';
                    Product['configuration'] = '16 GB';
                    Product['color'] = '';
                    Product['picture'] = '';
                    Product['active'] = 1;
                    Product['contract'] = '24 months';
                    Product['store'] = 'T-Mobile';

                    // get the attributes
                    var attributes = obj.custom_attributes;
                    for (var it in attributes) {
                        var ob = attributes[it];

                        // This code changes based on the version of Magento 
                        if (ob.atrribute_code && ob.atrribute_code.indexOf('thumbnail') > 0)       //TODO: RSG do not know how to get the path to the media folder
                            Product[ob.attribute_code] =cfg.magento.host  + '/pub/media/catalog/product/cache/image/265x265/beff4985b56e3afdbeabfc89641a4582/' + ob.value;

                        if (ob.attribute_code.indexOf('image') > 0)       //TODO: RSG do not know how to get the path to the media folder
                            Product[ob.attribute_code] =cfg.magento.host  + '/pub/media/catalog/product/cache/image/265x265/beff4985b56e3afdbeabfc89641a4582/' + ob.value;
                    }
                    productList.push(Product);
                    counter++;
                }
                callback(productList);
            });
    },




    /*
    * Get customer authentication token
    */
    getCustomerToken:function(callback){
        logger.info('Get Customer token with Magento Rest API');
        request({
            headers: {
                'Authorization': 'bearer ' + cfg.magento.auth_token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            uri: cfg.magento.host + cfg.magento.base_uri + '/integration/customer/token',
            body: {
                "username":"pallavi@plenarea.com",
                "password":"Pass@word30"
            },
            method: 'POST',
            json: true
        }, function (err, res, body) {
            if (err) {
                logger.error('Get customer token failure!! ' + JSON.stringify(err));
                callback(res);
            }
            else {
                logger.info('Get Customer Token successful.');
                logger.info('Response -- ' + JSON.stringify(body));
                magento_auth_token = body;
                callback(body);
            }
        });
    },

    /*
     *  Create a cart for customer user
     */

    createCart: function (token, callback) {
        logger.info('Create a new cart with Magento Rest API : ');
        request({
            headers: {
                'Authorization': 'bearer ' + token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            uri: cfg.magento.host + cfg.magento.base_uri + '/carts/mine',
            method: 'POST',
            json: true
        }, function (err, res, body) {
            if (err) {
                callback(res);
                logger.error('Create a cart in Magento failure!! ' + JSON.stringify(err));
            }
            else {
                logger.info('Create cart successful.');
                logger.info('Response -- ' + JSON.stringify(body));
                callback(body);
            }
        });
    },


    /*
    *  Add a single item to a cart. Cart object has sku,cartqty,name,retail_price,type_id
    */

    addItem2Cart: function (token, cart_id, item, callback) {
        logger.info('Add item to a cart');
        request({
            headers: {
                'Authorization': 'bearer ' + token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            uri: cfg.magento.host + cfg.magento.base_uri + '/carts/mine/items',
            method: 'POST',
            body : {"cartItem":{

                    "sku": item.sku,
                    "qty": item.qty,
                    "name": item.name,
                    "price":  item.retail_price,
                    "product_type":item.type_id,
                    "quote_id": '' + cart_id,
                    "product_option":
                        {"extension_attributes":
                                {
                                    "custom_options":[
                                        {"option_id":"thumbnail",
                                            "option_value":"\/d\/e\/devilmaycryiii3dantecosplay_1_.jpg"
                                        },
                                        {
                                            "option_id":"color_2",
                                            "option_value":"Red"
                                        },
                                        {
                                            "option_id":"google_size",
                                            "option_value":"xxs"}]
                                }
                        }
                }
            },
            json: true
        }, function (err, res, body) {
            if (err) {
                logger.error('Add item to cart into Magento failure!! ' + JSON.stringify(err));
                callback(res);

            }
            else {
                logger.info('Add item to cart into Magento successful.');
                logger.info('Response -- ' + JSON.stringify(body));
                callback(body);
            }
        });
    },


    /*
    * get shipping-methods
    */
    estimateShippingDetailsofCart: function(token, cart_id, customer ,callback) {
        logger.info("Estimate shipping details with Magento Rest API");
        var addressline1_2=new Array();
        addressline1_2.push('' + customer.shipaddress1);
        request({
            headers: {
                'Authorization': 'bearer ' + token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            uri: cfg.magento.host + cfg.magento.base_uri + '/carts/mine/estimate-shipping-methods',
            method: 'POST',
            body : {
                "address": {
                    "region": customer.shipstate,
                    "region_code": customer.shipstate,
                    "country_id": "US",
                    "street": addressline1_2,
                    "postcode": '' + customer.shipzip,
                    "city": '' + customer.shipcity,
                    "firstname": '' + customer.firstname,
                    "lastname": '' + customer.lastname,
                    "email": customer.email,
                    "telephone": '' + customer.phone,
                }
            },
            json: true
        }, function (err, res, body) {
            if (err) {
                logger.error('Estimate shipping details failed!! ' + JSON.stringify(err));
                callback(res);
            }
            else {
                logger.info('Estimate shipping details successful!!');
                logger.info('Response -- ' + JSON.stringify(body));
                callback(body);
            }
        });
    },


    /*
    *  Add shipping address to our cart
    */

    addShippingAddress2Cart: function (token, cart_id, customer, callback) {
        logger.info('Add shipping/billing address to our cart with Magento Rest API');
        var addressline1_2s=new Array();
        addressline1_2s.push('' + customer.shipaddress1);
        var addressline1_2b=new Array();
        addressline1_2b.push('' + customer.billaddress1);
        request({
            headers: {
                'Authorization': 'bearer ' + token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            uri: cfg.magento.host + cfg.magento.base_uri + '/carts/mine/shipping-information',
            method: 'POST',
            body : {

                "cart_id": '' + cart_id,
                "addressInformation": {
                    "shippingAddress": {
                        "region": customer.shipstate,
                        //"region_id": 1,         //TODO: RSG How do we know region_id
                        "country_id": "US",
                        "street": addressline1_2s,
                        "company": "abc",
                        "telephone": '' + customer.phone,
                        "postcode": '' + customer.shipzip,
                        "city": '' + customer.shipcity,
                        "firstname": '' + customer.firstname,
                        "lastname": '' + customer.lastname,
                        "email": customer.email,
                        "prefix": "address_",
                        "region_code": '' + customer.shipstate,
                        "sameAsBilling": 1
                    },
                    "billingAddress": {
                        "region": customer.billstate,
                        //"region_id": 1,     //TODO: RSG How do we know region_id
                        "country_id": "US",
                        "street": addressline1_2b,
                        "company": "abc",
                        "telephone": '' + customer.phone,
                        "postcode": '' + customer.billzip,
                        "city": '' + customer.billcity,
                        "firstname": '' + customer.firstname,
                        "lastname": '' + customer.lastname,
                        "email": customer.email,
                        "prefix": "address_",
                        "region_code": '' + customer.billstate
                    },
                    "shipping_method_code": customer.shipping_method_code,
                    "shipping_carrier_code": customer.shipping_carrier_code
                }
            },
            json: true
        }, function (err, res, body) {
            if (err) {
                logger.info('Add of shipping address into Magento failed!! ' + JSON.stringify(err));
                callback(res);

            }
            else {
                logger.info('Adding of shipping address to the cart is successful.');
                logger.info('Response -- ' + JSON.stringify(body));
                callback(body);
            }
        });
    },

    /*
    *  create order
    */

    createOrder: function (token, cart_id, customer, callback) {
        logger.info('Create order from cart with Magento Rest API');

        var addressline1_2=new Array();
        addressline1_2.push('' + customer.billaddress1);
        request({
            headers: {
                'Authorization': 'bearer ' + token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            uri: cfg.magento.host + cfg.magento.base_uri + '/carts/mine/payment-information',
            method: 'POST',
            body : {
                "paymentMethod": {
                    "method": "checkmo"
                },
                "billing_address": {

                    "email": customer.email,
                    "region": customer.billcity,
                    "region_id": 1,         //TODO: RSG How do we know region_id
                    "region_code": '' + customer.billstate,
                    "country_id": "US",
                    "street": addressline1_2,
                    "postcode": '' + customer.billzip,
                    "city": '' + customer.billcity,
                    "telephone": '' + customer.phone,
                    "firstname": '' + customer.firstname,
                    "lastname": '' + customer.lastname
                }
            },
            json: true
        }, function (err, res, body) {
            if (err) {
                callback(res);
                logger.info('Create order from cart into Magento failed! ' + JSON.stringify(err));
            }
            else {
                logger.info('Create order from cart is successful!!');
                logger.info('Response -- ' + JSON.stringify(body));
                callback(body);
            }
        });
    },



};
