var cfg = require('../config/index');
var logger = require('../utility/logger');
var myip = require('../utility/ip');
var request = require('request');
var CryptoJS = require('crypto-js');


module.exports = {


    /*
     *  Send Magento Order to BIF
     */
    sendMagentoOrder: function (auth_token, order, callback) {
        var message = 'Submit order to BIF failed!';
        var response = {'message': message, 'error': 500};

        var bifOrder = this.createBIFOrder(order);
        logger.debug('BIF order -- ' + JSON.stringify(bifOrder));
        
        var dateString = this.getDateStandardFormat();
        var auth_hash =  this.getAuthHashToken(dateString);
        
        var bif_uri = cfg.bif.host + cfg.bif.base_uri;
        logger.debug('Attempting to communicate with BIF system with endpoint -- ' + bif_uri);
        
        request({
            headers: {
                'Authorization': auth_hash,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-bif-client-id': cfg.bif.clientId,
                'x-bif-date' : dateString
            },
            uri: bif_uri,
            method: 'POST',
            body:  bifOrder,
            json: true
        }, function (err, res, body) {
            if (err) {
                callback(response);
                logger.error('Submit order to BIF failed!');
                logger.error(JSON.stringify(err));
            }
            else {
                logger.info('Submit order to BIF successful!!');
                logger.debug('Response -- ' + JSON.stringify(res));
                callback(body);
            }
        });
    },


    /*
     *  Create brand new auth hash token for BIF
     */
    getAuthHashToken: function (dateString) {

        var shaKey = cfg.bif.shaKey;
        var clientId = cfg.bif.clientId;
        var data = clientId + dateString;
        return CryptoJS.HmacSHA1(data, shaKey).toString(CryptoJS.enc.Base64);
    },


    /*
     *  Get date string as per BIF 1.3 specs
     */
    getDateStandardFormat: function () {

        var d = new Date();
        return d.getFullYear() + '-' +  (d.getMonth() + 1) + '-' + d.getDate() + ' ' + d.getHours() + ':'
            + d.getMinutes() + ':' + d.getSeconds().toString().substr(0,2) + '.' + d.getMilliseconds() + '000';
    },
    

    /*
     *  Create a BIF order from Magento Order
     */
    createBIFOrder: function (order) {

        var bifjson = {
            orders: [],
            clientId: cfg.bif.clientId,
            clientOrderBatchNumber: cfg.bif.clientOrderBatchPrefix + order.entity_id,  // ??
            Extensions : []
        };
        if (myip.IP != null) {
            myip.IP.forEach(function(item) {
                var o = { "name": "IP", "value" : item };
                bifjson.Extensions.push(o);
            });
        }
        var clientOrderNumberMagento = cfg.magento.orderPrefix + order.entity_id;
        var shipping_assignments = order.extension_attributes.shipping_assignments[0];
        var address = shipping_assignments.shipping.address;

        var biforder = {
            clientOrderNumber: clientOrderNumberMagento,
            programID: cfg.magento.programId,
            participantID: "",
            shipToFirstName: address.firstname,
            shipToLastName: address.lastname,
            shipToAddressLine1: address.street[0],
            shipToAddressLine2: address.street[1],
            shipToCity: address.city,
            shipToState: address.region_code,
            shipToPostalCode: address.postcode,
            shipToCountry: address.country_id,
            shipToPhone: address.telephone,
            shipToEmail: address.email,
            shipToCompanyName: "",
            orderLineItem : []
        };

	    var i = 1;
	    if (order.items != null) {
            order.items.forEach(function(item) {
                if (typeof item.parent_item  == "undefined") {
                    var sku = item.sku; // TODO: Pranabesh, This is a temporary fix due to Magento line item defects!
                    var product_code, package_id;
                    var arrInfo = sku.split(',');
                    if(arrInfo.length > 2) {
                        product_code = arrInfo[0].trim();
                        package_id = arrInfo[1].trim();
                    } else {
                        product_code = cfg.magento.product.offerCode;
                        package_id = cfg.magento.product.packageId;
                    }
                    biforder.orderLineItem.push({
                        clientLineItemNumber: '' + i,
                        offerCode: product_code,
                        packageID: package_id,
                        quantity: item.qty_ordered,
                        shippingMethod: item.shipping_description,
                        denomination: item.price
                    });
                    i++;
                }
	        });
        }
    
        bifjson.orders.push(biforder);
        return bifjson;
    }

};
