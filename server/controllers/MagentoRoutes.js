var magentoService = require('../services/MagentoService');
var Joi = require('joi');

module.exports=[

    {
        method: 'GET',
        path: '/magento/order/{orderid}',
        config: {
            auth: false,
            handler: magentoService.getMagentoOrderById,
            description: 'Get an order from Magento',
            notes: 'A GET request to retrieve a single order from Magento',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                params: Joi.object({
                    'orderid': Joi.string().required()
                }).unknown(),
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    },

    {
        method: 'GET',
        path: '/magento/order/{orderid}/comments',
        config: {
            auth: false,
            handler: magentoService.getMagentoOrderCommentsByOrderId,
            description: 'Get all associated comments for an order from Magento',
            notes: 'A GET request to get all comments for a single order from Magento',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                params: Joi.object({
                    'orderid': Joi.string().required()
                }).unknown(),
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    },

    {
        method:'GET',
        path:'/magento/products',
        config: {
            auth: false,
            handler: magentoService.findAllMagentoProducts,
            description: 'Get all Magento products',
            notes: 'Returns all the products from our Magento',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    },


    {
        method:'GET',
        path:'/magento/products/category/{categoryid}',
        config: {
            auth: false,
            handler: magentoService.findAllMagentoCategorywiseProducts,
            description: 'Get all Magento categorywise products',
            notes: 'Returns all the categorywise products from our Magento',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    },


    {
        method: 'POST',
        path: '/magento/checkout/cart',
        config: {
            auth: false,
            handler: magentoService.submitCart,
            description: 'Submit full cart from UI to Magento',
            notes: 'A POST request to submit full cart from UI to Magento. This shuld create an order.',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                payload: {
                    customer: Joi.object().required(),
                    cart: Joi.object().required()
                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    }

];