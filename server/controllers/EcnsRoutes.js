var ecnsService = require('../services/EcnsService');
var Joi = require('joi');


module.exports=[

    {
        method:'POST',
        path:'/ecns/sendEmail',
        config: {
            auth: false,
            handler: ecnsService.sendEmail,
            description: 'Submit a request to send an email using ECNS',
            notes: 'A POST request to submit message to ECNS for outbound email notification',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                payload: {
                    from: Joi.string().required(),
                    to: Joi.string().required(),
                    subject: Joi.string().required(),
                    message: Joi.string().required(),
                    reference_type: Joi.string().required(),
                    reference_reqnum: Joi.string().required()
                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    }
];
