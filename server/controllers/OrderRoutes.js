var orderService = require('../services/OrderService');
var Joi = require('joi');


module.exports=[

    {
        method: 'POST',
        path: '/bif/order/notification',
        config: {
            auth: false,
            handler: orderService.processNotification,
            description: 'Process inbound status notifications from BIF',
            notes: 'A POST request to submit status notification for an order by BIF.',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                payload: {
                    batchId: Joi.string().required(),
                    fulfillmentTimeStamp: Joi.string().optional(),
                    orders: Joi.array().required()
                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    }
];