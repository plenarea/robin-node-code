var securePayService = require('../services/SecurePayService');
var Joi = require('joi');


module.exports=[

    {
        method: 'POST',
        path: '/securepay/iframeurl',
        config: {
            auth: false,
            handler: securePayService.getSecurePayURL,
            description: 'Get complete iframe url details and source attributes from Gateway Service',
            notes: 'A POST request to get iframe URL from SecurePay, uses fingerprint and TripleDES based on SecurePay Web Portal 1.4 specs',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                payload: {
                    order_id: Joi.string().required(),
                    amount: Joi.string().required(),
                    test: Joi.string()
                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    },

    {
        method: 'POST',
        path: '/securepay/inbound/saveauth',
        config: {
            auth: false,
            handler: securePayService.processSaveAuth,
            description: 'Save return response from SecurePay after auth is complete at check out',
            notes: 'A POST request to save authtoken, transaction number, timestamp, dollar amount for a given order_id (entity_id) after SecurePay iframe integration',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                payload: {
                    authtoken: Joi.string().required(),
                    transactionnum: Joi.string().required(),
                    order_id: Joi.string().required(),
                    timestamp: Joi.string().required(),
                    amount: Joi.string().required()
                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    },

    {
        method: 'POST',
        path: '/securepay/card/auth',
        config: {
            auth: false,
            handler: securePayService.processCreditCardAuth,
            description: 'Submit a card for authorization (Mock)',
            notes: 'A POST request to send card details to SecurePay based on SecurePay Web Portal 1.4 specs',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                payload: {
                    creditcard: Joi.string().required(),
                    expiration: Joi.string().required(),
                    nameoncard: Joi.string().required(),
                    securitycode: Joi.string().required(),
                    zip: Joi.string().required(),
                    amount: Joi.string().required()
                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    },

    {
        method: 'POST',
        path: '/securepay/outbound/postpayment',
        config: {
            auth: false,
            handler: securePayService.postPartialPayment,
            description: 'Post a payment to SecurePay, called internally for shipped orders / line items',
            notes: 'A POST request to submit a transaction for actual payment to SecurePay based on SecurePay Web Portal 1.4 specs',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                payload: {

                    authtoken: Joi.string().required(),
                    order_id: Joi.string().required(),
                    amount: Joi.string().required()
                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    },

    {
        method: 'POST',
        path: '/securepay/outbound/postrefund',
        config: {
            auth: false,
            handler: securePayService.postRefund,
            description: 'Post a refund to SecurePay',
            notes: 'A POST request to submit a transaction for actual refund to SecurePay based on SecurePay Web Portal 1.4 specs',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                payload: {
                    authtoken: Joi.string().required(),
                    transaction_id: Joi.string().optional(),
                    order_id: Joi.string().required(),
                    amount: Joi.string().required()
                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    },

    {
        method: 'POST',
        path: '/securepay/outbound/postvoid',
        config: {
            auth: false,
            handler: securePayService.postVoid,
            description: 'Post a void to SecurePay',
            notes: 'A POST request to submit a transaction for voiding to SecurePay based on SecurePay Web Portal 1.4 specs',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                payload: {
                    authtoken: Joi.string().required(),
                    order_id: Joi.string().required(),
                    transaction_id: Joi.string().optional(),
                    amount: Joi.string().required()
                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    }

];