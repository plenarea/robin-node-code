var bifService = require('../services/BifService');
var Joi = require('joi');


module.exports=[

    {
        method: 'POST',
        path: '/bif/order',
        config: {
            auth: false,
            handler: bifService.sendMagentoOrder,
            description: 'Submit a Magento order to BIF',
            notes: 'A POST request to send a Magento order to BIF for fulfillment.',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                payload: {
                    order: Joi.string().required()
                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    }
];