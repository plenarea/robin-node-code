# README #

This is the server side code for integration service gateway


* Version 1.0


### How do I get set up? ###

* Summary of set up

* Configuration

    In order for this code to run, you need at-least 6.3+ and higher version of node installed.
    Also npm is a must on your machine, so
    Once you get or clone the code go to the gateway_service directory and then do 
    $npm install
    This command should get all the packages neccesary to run. It reads package.json for dependency.Might take 3/4 minutes!

* Dependencies

* How to run all tests
    $npm test

* How to run a single test
    npm run test --specs spec/ping-spec.js
    npm run test --specs spec/magento-create-order-spec.js

* Run instructions
    $node server  (run locally. this will block your console)

    $nohup node server >> /tmp/log/server.log 2>&1 &  (run in background and release you console tty)



###How to install this app by pushing it to your Cloud Foundry and binding with the MySQL service

Example:

     $ cf push isg-nodejs-app -c 'bundle exec node server -p $PORT'
     $ cf create-service p-mysql 100mb-dev mydb
     $ cf bind-service isg-nodejs-app mydb
     $ cf restart isg-nodejs-app

### Endpoints
* Team contact# Robin Ghosh (rghosh@incomm.com)

